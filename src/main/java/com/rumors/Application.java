package com.rumors;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/** Rumors. Alice is throwing a party with N other guests, including Bob. Bob starts a rumor
 * about Alice by telling it to one of the other guests. A person hearing this rumor for the
 * first time will immediately tell it to one other guest, chosen at random from all the people
 * at the party except Alice and the person from whom they heard it. If a person (including Bob)
 * hears the rumor for a second time, he or she will not propagate it further.
 * Write a program to estimate the probability that everyone at the party (except Alice)
 * will hear the rumor before it stops propagating.
 * Also calculate an estimate of the expected number of people to hear the rumor.
 */
public class Application {
     /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

        System.out.println("Please enter how many people will be at party excluding Alice ");
        System.out.print("The number must be > 2: ");

        Scanner scanner = new Scanner(System.in);

        int guestsCount = scanner.nextInt();

        List<Guest> guests = new ArrayList();

        for (int i = 0; i < guestsCount; i++) {
            guests.add(new Guest());
        }

        Rumors rumors = new Rumors(guests);

        double probabilityAll = rumors.probabilityFullSpreadRumor();

        int generalNumberOfHeard = 0;
        for (int i = 0; i < 1000; i++) {
            rumors.partyInit();
            generalNumberOfHeard += rumors.numberOfPeopleHeardRumor();
        }

        double averageWhoHeard = (double) generalNumberOfHeard/1000;


        System.out.printf("Expected who heard = %.3f People; Probability All = %.5f%%", averageWhoHeard, probabilityAll);
    }


}

