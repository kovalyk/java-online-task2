package com.rumors;

import java.util.List;
import java.util.Random;



/**
 * This purpose of this class is to define the number of people
 * who heard the rumors
 */
public class Rumors {

    private List<Guest> guests;

    /**
     * Apply to all the guests that they didn't hear rumor
     */
    public void partyInit() {
        for (int i = 0; i < guests.size(); i++) {
            guests.get(i).setHeard(false);
        }
    }

    /**
     * Probability full spread rumor double.
     *
     * @return the double
     */
    public double probabilityFullSpreadRumor() {
        int guestCount = guests.size();
        double probability = 1.0;

        for (int i = 1; i <= guestCount - 2; i++) {
            probability *= (double) i / (guestCount - 2);
        }
        return probability;
    }


    /**
     * Number of people who heard rumor at the one party
     *
     * @return the int
     */
    public int numberOfPeopleHeardRumor() {
        int countOfHeardPeople = 1;
        Guest currentGuest = guests.get(0);
        currentGuest.setHeard(true);

        for (int i = 0; i < guests.size(); i++) {
            Guest previousGuest = currentGuest;

            do {
                Random random = new Random();
                currentGuest = guests.get(random.nextInt(guests.size()));
            } while (currentGuest.equals(previousGuest));

            if (!currentGuest.isHeard()) {
                currentGuest.setHeard(true);
                countOfHeardPeople++;
            } else {
                return countOfHeardPeople;
            }
        }
        return countOfHeardPeople;
    }

    /**
     * Instantiates a new Party with guests.
     *
     * @param guests the guests
     */
    public Rumors(List<Guest> guests) {
        this.guests = guests;
    }

    /**
     * Gets all guests at the party.
     *
     * @return the guests
     */
    public List<Guest> getGuests() {
        return guests;
    }

    /**
     * Sets guests at party.
     *
     * @param guests the guests
     */
    public void setGuests(List<Guest> guests) {
        this.guests = guests;
    }
}


