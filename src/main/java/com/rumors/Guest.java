package com.rumors;

/**
 * @author Ihor Kovalyk
 * @version 1.0
 * @since 2019-07-30
 */
public class Guest {

     private boolean isHeard;

    /**
     * Returns if the rumor is heard.
     *
     * @return the boolean
     */
    public boolean isHeard() {
        return isHeard;
    }

    /**
     * Sets heard rumor.
     *
     * @param heard the heard
     */
    public void setHeard(boolean heard) {
        isHeard = heard;
    }
}
